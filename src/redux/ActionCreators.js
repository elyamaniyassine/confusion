import { actionType } from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

export const addFeedback = (feedback) => ({
    type: actionType.ADD_FEEDBACK,
    payload: feedback
});

export const postFeedback = (firstname, lastname, telnum, email, agree, contactType, message) => (dispatch) => {
    const newfeedback = {
        firstname: firstname,
        lastname: lastname,
        telnum: telnum,
        email: email ,
        agree: agree,
        contactType: contactType,
        message:message
    }
    return fetch(baseUrl + 'feedback', {
        method: 'POST',
        body: JSON.stringify(newfeedback),
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                }
                else {
                    var error = new Error('Error ' + response.status + ':' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(response => dispatch(addFeedback(response)))
        .then(response => alert('thank you for your feedback! \n'+ JSON.stringify(response.payload)))
        .catch(error => {
            console.log('post Comments ', error.message);
            alert('your comment could not be posted\n Error : ' + error.message)
        })

}

export const addComment = (comment) => ({
    type: actionType.ADD_COMMENT,
    payload: comment
});

export const postComment = (dishId, rating, author, comment) => (dispatch) => {

    const newComment = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment,
    }
    newComment.date = new Date().toISOString();
    return fetch(baseUrl + 'comments', {
        method: 'POST',
        body: JSON.stringify(newComment),
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin'
    })
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ':' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(response => dispatch(addComment(response)))
        .catch(error => {
            console.log('post Comments ', error.message);
            alert('your comment could not be posted\n Error : ' + error.message)
        })
}

export const fetchDishes = () => (dispatch) => {

    dispatch(dishesLoading(true));

    return fetch(baseUrl + 'dishes')
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ':' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(dishes => dispatch(addDishes(dishes)))
        .catch(error => dispatch(dishesFailed(error.message)))
}

export const dishesLoading = () => ({
    type: actionType.DISHES_LOADING
});

export const dishesFailed = (errmess) => ({
    type: actionType.DISHES_FAILED,
    payload: errmess
});

export const addDishes = (dishes) => ({
    type: actionType.ADD_DISHES,
    payload: dishes
});

export const fetchComments = () => (dispatch) => {
    return fetch(baseUrl + 'comments')
        .then(response => {
                if (response.ok) {
                    return response;
                } else {
                    var error = new Error('Error ' + response.status + ': ' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(comments => dispatch(addComments(comments)))
        .catch(error => dispatch(commentsFailed(error.message)))

}
export const addComments = (comments) => ({
    type: actionType.ADD_COMMENTS,
    payload: comments
});

export const commentsFailed = (errmess) => ({
    type: actionType.COMMENTS_FAILED,
    payload: errmess
});

export const fetchPromos = () => (dispatch) => {
    dispatch(promosLoading(true));

    return fetch(baseUrl + 'promotions')
        .then(response => {
                if (response.ok) {
                    return response;
                }

                else {
                    var error = new Error('Error ' + response.status + ':' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },

            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(promos => dispatch(addPromos(promos)))
        .catch(error => dispatch(promosFailed(error.message)))

}

export const promosLoading = () => ({
    type: actionType.PROMOS_LOADING
});

export const promosFailed = (errmess) => ({
    type: actionType.PROMOS_FAILED,
    payload: errmess
});

export const addPromos = (promos) => ({
    type: actionType.ADD_PROMOS,
    payload: promos
});

export const fetchLeaders = () => (dispatch) => {
    dispatch(leadersLoading(true));

    return fetch(baseUrl + 'leaders')
        .then(response => {
                if (response.ok) {
                    return response;
                }
                /*server response with error like  500*/
                else {
                    var error = new Error('Error ' + response.status + ':' + response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            /*server doesn't response*/
            error => {
                var errmess = new Error(error.message);
                throw errmess
            })
        .then(response => response.json())
        .then(leaders => dispatch(addLeaders(leaders)))
        .catch(error => dispatch(leadersFailed(error.message)))

}

export const leadersLoading = () => ({
    type: actionType.LEADERS_LOADING
});

export const leadersFailed = (errmess) => ({
    type: actionType.LEADERS_FAILED,
    payload: errmess
});

export const addLeaders = (leaders) => ({
    type: actionType.ADD_LEADERS,
    payload: leaders
});