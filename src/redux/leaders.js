import {actionType} from './ActionTypes';

export const Leaders = (state = {
    isLoading:true,
    errMess : null,
    leaders : []
}, action) => {
    switch (action.type) {

        case actionType.ADD_LEADERS:
            return {...state,isLoading : false, errMess:null, leaders:action.payload}

        case actionType.LEADERS_LOADING:
            return {...state,isLoading : true, errMess:null, leaders:[]}

        case actionType.Leaders_FAILED:
            return {...state,isLoading : false, errMess:action.payload, leaders:[]}

        default:
            return state;
    }
};